package ru.tsc.avramenko.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.avramenko.tm.component.Bootstrap;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.exception.empty.EmptyIdException;
import ru.tsc.avramenko.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.avramenko.tm.exception.system.ProcessException;
import ru.tsc.avramenko.tm.model.Project;
import ru.tsc.avramenko.tm.model.Session;
import ru.tsc.avramenko.tm.service.ConnectionService;
import ru.tsc.avramenko.tm.service.PropertyService;
import ru.tsc.avramenko.tm.service.SessionService;

import java.util.List;

public class ProjectRepositoryTest {

    @Nullable
    private ProjectRepository projectRepository;

    @Nullable
    private SessionService sessionService;

    @NotNull
    private Bootstrap bootstrap = new Bootstrap();

    @Nullable
    private Project project;

    @Nullable
    private Session session;

    @NotNull
    protected static final String TEST_PROJECT_NAME = "TestName";

    @NotNull
    protected static final String TEST_DESCRIPTION_NAME = "TestDescription";

    @NotNull
    protected static final String TEST_USER_ID_INCORRECT = "TestUserIdIncorrect";

    @Before
    public void before() {
        ConnectionService connectionService = new ConnectionService(new PropertyService());
        sessionService = new SessionService(connectionService, bootstrap);
        this.session = sessionService.open("Test", "Test");
        projectRepository = new ProjectRepository(connectionService.getConnection());
        project = new Project();
        project.setName(TEST_PROJECT_NAME);
        project.setDescription(TEST_DESCRIPTION_NAME);
        project = projectRepository.add(session.getUserId(), project);
    }

    @After
    public void after() {
        sessionService.close(session);
    }

    @Test
    public void add() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        Assert.assertNotNull(project.getName());
        Assert.assertNotNull(project.getDescription());
        Assert.assertEquals(TEST_PROJECT_NAME, project.getName());
        Assert.assertEquals(TEST_DESCRIPTION_NAME, project.getDescription());

        @Nullable final Project projectById = projectRepository.findById(project.getId());
        Assert.assertNotNull(projectById);
    }

    @Test
    public void findAll() {
        @NotNull final List<Project> projects = projectRepository.findAll();
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void existsById() {
        Assert.assertTrue(projectRepository.existsById(session.getUserId(), project.getId()));
    }

    @Test
    public void findAllByUserId() {
        @NotNull final List<Project> projects = projectRepository.findAll(session.getUserId());
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void findAllByUserIdIncorrect() {
        @NotNull final List<Project> projects = projectRepository.findAll(TEST_USER_ID_INCORRECT);
        Assert.assertNotEquals(1, projects.size());
    }

    @Test
    public void findById() {
        @Nullable final Project project = projectRepository.findById(session.getUserId(), this.project.getId());
        Assert.assertNotNull(project);
    }

    @Test
    public void findByIndex() {
        @Nullable final Project project = projectRepository.findByIndex(session.getUserId(), 1);
        Assert.assertNotNull(project);
    }

    @Test
    public void removeByName() {
        projectRepository.removeByName(session.getUserId(), TEST_PROJECT_NAME);
        Assert.assertNull(projectRepository.findByName(session.getUserId(), this.project.getName()));
    }

    @Test
    public void removeById() {
        projectRepository.removeById(session.getUserId(), project.getId());
        Assert.assertNull(projectRepository.findById(project.getId()));
    }

}