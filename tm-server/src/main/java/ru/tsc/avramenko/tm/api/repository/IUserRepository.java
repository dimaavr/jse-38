package ru.tsc.avramenko.tm.api.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.IRepository;
import ru.tsc.avramenko.tm.model.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    @SneakyThrows
    User findByLogin(@NotNull String login);

    @Nullable
    @SneakyThrows
    User findByEmail(@NotNull String email);

    @Nullable
    @SneakyThrows
    void removeUserByLogin(@NotNull String login);

    @Nullable
    @SneakyThrows
    User add(@Nullable User entity);

    @Nullable
    @SneakyThrows
    User update(@Nullable User entity);


}